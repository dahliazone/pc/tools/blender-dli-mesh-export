#!BPY

"""
Name: 'Elibgl'
Blender: 244
Group: 'Export'
Tooltip: 'Elibgl Mesh exporter'
"""
import Blender
import bpy
import struct

# Functions to do conversion from float to bytes
def floatToBytes(f):
	return struct.pack(">f", f)

def bytesToFloats(b):
	return struct.unpack(">f", b)

class ElibglSubmesh:
	def __init__(self):
		self.name = ""
		self.tris = []
		self.quads = []
		self.tlayers = None
		self.material = ""

class ElibglFace:
	def __init__(self):
		self.vrts = []
		self.tcrds = []
		self.smooth = None

class ElibglMeshExporter:

	def __init__(self):
		# Preload object and it's mesh
		scene = bpy.data.scenes.active
		self.obj = scene.objects.active
		if self.obj.getType() != "Mesh":
			# Todo: Throw exception!
			print "ERROR: Selected object must be mesh!"
			return
		self.mesh = self.obj.getData(mesh=1)

		# Initialize list for names of submeshes
		self.submeshes = []
		for submesh_mat in self.mesh.materials:
			new_submesh = ElibglSubmesh()
			new_submesh.material = submesh_mat.name
			self.submeshes.append(new_submesh)

	def export(self, filename):
		print "Now exporting..."
		efile = open(filename, "w")


		# ------
		# Header
		# ------

		# Identification information
		efile.write("ELIBGLMESH")

		# Version
		efile.write("             1.0")




		# -------------------
		# Export Vertexgroups
		# -------------------

		# While exporting vertices, use the following list to find correct
		# vertexgroups for them
		vg_names = self.mesh.getVertGroupNames()
		efile.write(chr((len(vg_names) >> 24) & 0xFF))
		efile.write(chr((len(vg_names) >> 16) & 0xFF))
		efile.write(chr((len(vg_names) >> 8) & 0xFF))
		efile.write(chr((len(vg_names) >> 0) & 0xFF))
		for vg_name in vg_names:
			vg_name_len = len(vg_name)
			if (vg_name_len > 255):
				print "Name of vertexgroup \"%s\" is too long! The maximum length is 255." % (vg_name)
				return
			if (vg_name_len == 0):
				print "Found vertexgroup with no name!"
				return
			efile.write(chr(vg_name_len))
			efile.write("%s" % (vg_name))


		# ---------------
		# Export Vertices
		# ---------------

		efile.write(chr((len(self.mesh.verts) >> 24) & 0xFF))
		efile.write(chr((len(self.mesh.verts) >> 16) & 0xFF))
		efile.write(chr((len(self.mesh.verts) >> 8) & 0xFF))
		efile.write(chr((len(self.mesh.verts) >> 0) & 0xFF))
		for vrt in self.mesh.verts:
			efile.write(floatToBytes(vrt.co.x))
			efile.write(floatToBytes(vrt.co.y))
			efile.write(floatToBytes(vrt.co.z))
			# Before writing weights to file calculate their sum so each weight
			# can be scaled to value between 0.0 and 1.0
			total_weight = 0.0
			non_zero_weights = 0
			for vg_p in self.mesh.getVertexInfluences(vrt.index):
				if vg_p[1] > 0.0:
					total_weight += vg_p[1]
					non_zero_weights += 1
			efile.write(chr(non_zero_weights))
			if non_zero_weights > 255:
				print "Found vertex that belongs to too many vertexgroups! Maximal amount of vertexgroups is 255."
				return;
			elif non_zero_weights > 0:
				for vg_p in self.mesh.getVertexInfluences(vrt.index):
					if vg_p[1] > 0.0:
						vg_p_name = vg_p[0]
						vg_p_weight = vg_p[1] / total_weight
						# Now find the correct vertexgroup
						vg_p_index = None
						index_seek = 0
						for name in vg_names:
							if name == vg_p_name:
								vg_p_index = index_seek
							index_seek += 1
						if vg_p_index == None:
							print "Unable to find vertexgroup \"%s\"!" % (vg_p_name)
							return
						efile.write(chr(vg_p_index))
						efile.write(floatToBytes(vg_p_weight))


		# ----------------
		# Export Submeshes
		# ----------------

		# Before exporting submeshes gather faces to correct submeshes
		for face in self.mesh.faces:
			new_face = ElibglFace()
			# Store vertices
			for v in face.verts:
				new_face.vrts.append(v.index)
			# Store texturecoordinates
			if self.mesh.faceUV:
				for uv in face.uv:
					new_face.tcrds.append(uv.x)
					new_face.tcrds.append(uv.y)
			# Store smooth
			new_face.smooth = (face.smooth != 0)
			# Store face to submesh
			if len(face.verts) == 3:
				self.submeshes[face.mat].tris.append(new_face)
			elif len(face.verts) == 4:
				self.submeshes[face.mat].quads.append(new_face)
			else:
				print "Unknown face with %i vertices!" % (len(face.verts))
				return
			# Store number of texture layers if it hasn't
			# been stored yet
			if self.submeshes[face.mat].tlayers == None:
				#if len(face.uv) % len(face.verts) != 0:
				#	print "Invalid number of texturelayers!"
				#	return
				#self.submeshes[face.mat].tlayers = len(face.uv) / len(face.verts)
				self.submeshes[face.mat].tlayers = self.mesh.faceUV

		submeshes_mats = self.mesh.materials
		efile.write(chr((len(submeshes_mats) >> 24) & 0xFF))
		efile.write(chr((len(submeshes_mats) >> 16) & 0xFF))
		efile.write(chr((len(submeshes_mats) >> 8) & 0xFF))
		efile.write(chr((len(submeshes_mats) >> 0) & 0xFF))
		for submesh in self.submeshes:

			# Name
			submesh_name_len = len(submesh.name)
			if submesh_name_len > 255:
				print "Name of submesh \"%s\" is too long! The maximum length is 255." % (submesh_name)
				return
			efile.write(chr(submesh_name_len))
			efile.write("%s" % (submesh.name))

			# Default material
			submesh_material_len = len(submesh.material)
			if submesh_material_len > 255:
				print "Material of submesh \"%s\" is too long! The maximum length is 255." % (submesh_material)
				return
			efile.write(chr(submesh_material_len))
			efile.write("%s" % (submesh.material))

			# Amount of texturelayers
			if submesh.tlayers > 255:
				print "Too many texturelayers!"
				return
			efile.write(chr(submesh.tlayers));

			# Faces. First triangles
			efile.write(chr((len(submesh.tris) >> 24) & 0xFF))
			efile.write(chr((len(submesh.tris) >> 16) & 0xFF))
			efile.write(chr((len(submesh.tris) >> 8) & 0xFF))
			efile.write(chr((len(submesh.tris) >> 0) & 0xFF))
			for tri in submesh.tris:
				# Vertices
				for v in tri.vrts:
					efile.write(chr((v >> 24) & 0xFF))
					efile.write(chr((v >> 16) & 0xFF))
					efile.write(chr((v >> 8) & 0xFF))
					efile.write(chr((v >> 0) & 0xFF))
				# Texturecoordinates
				if len(tri.tcrds) != submesh.tlayers * 3 * 2:
					print "Invalid triangle with %i texturecoordinates using %i texturelayers!" % (len(tri.tcrds), submesh.tlayers)
					return
				for tcrd in tri.tcrds:
					efile.write(floatToBytes(tcrd))
				# Flags
				flags = 0
				if tri.smooth:
					flags += 1
				efile.write(chr(flags));
			# Then quads
			efile.write(chr((len(submesh.quads) >> 24) & 0xFF))
			efile.write(chr((len(submesh.quads) >> 16) & 0xFF))
			efile.write(chr((len(submesh.quads) >> 8) & 0xFF))
			efile.write(chr((len(submesh.quads) >> 0) & 0xFF))
			for quad in submesh.quads:
				# Vertices
				for v in quad.vrts:
					efile.write(chr((v >> 24) & 0xFF))
					efile.write(chr((v >> 16) & 0xFF))
					efile.write(chr((v >> 8) & 0xFF))
					efile.write(chr((v >> 0) & 0xFF))
				# Texturecoordinates
				if len(quad.tcrds) != submesh.tlayers * 4 * 2:
					print "Invalid quad with %i texturecoordinates using %i texturelayers!" % (len(quad.tcrds), submesh.tlayers)
					return
				for tcrd in quad.tcrds:
					efile.write(floatToBytes(tcrd))
				# Flags
				flags = 0
				if quad.smooth:
					flags += 1
				efile.write(chr(flags));

			#submeshes_id += 1


		# ------
		# Finish
		# ------

		efile.close()
		print "Exporting finished succesfully!"


exporter = ElibglMeshExporter()

Blender.Window.FileSelector(exporter.export, "Export")

