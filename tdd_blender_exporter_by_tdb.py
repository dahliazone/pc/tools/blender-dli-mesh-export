#!BPY

"""
Name: 'Three-D Data (.tdd) ...'
Blender: 237
Group: 'Export'
"""

import Blender
import math
import copy

class TddVertex:
	def __init__(self):
		self.p=[0,0,0]
		self.n=None
		self.t=None
		self.l=None

def create_vector(p1, p2):
	return [p2[0]-p1[0],p2[1]-p1[1],p2[2]-p1[2]]

def get_global_location(obj):
	return (Blender.Mathutils.Vector([0,0,0,1])*obj.getMatrix())[0:3]

class TddFace:
	def __init__(self):
		self.v=[]
		self.mat=None
		self.smooth=None
		self.normal=None

class TddExporter:
	def __init__(self):
		self.objects=True
		self.lights=True
		self.paths=False
		self.cameras=True
		self.use_basenames=True
		self.selected_only=False
		self.layer=""

	def export(self, filename):
		print "Exporting Three-D Data..."
		self.out=open(filename,"w")

		scene=Blender.Scene.getCurrent()
		objects=[]
		if self.objects:
			if self.selected_only:
				objects=Blender.Object.GetSelected()
			else:
				objects=scene.getChildren()

		materials=[]
		if self.objects:
			for i in objects:
				if i.getType()=="Mesh":
					mesh=i.getData()
					for j in mesh.getMaterials():
						if j.getName() not in map(lambda x: x.getName() , materials):
							materials.append(j)

		textures=[]
		for i in materials:
			for j in i.getTextures():
				if j and j.tex not in textures:
					textures.append(j.tex)

		for i in textures:
			self.export_texture(i)
		for i in materials:
			self.export_material(i)
		for i in objects:
			if i.getType()=="Mesh" and self.objects:
				self.export_mesh(i)
			elif i.getType()=="Lamp" and self.lights:
				self.export_light(i)
			elif i.getType()=="Curve" and self.paths:
				self.export_path(i)
			elif i.getType()=="Camera" and self.cameras:
				self.export_camera(i)
		print "Done."

	def export_texture(self, tex):
		if tex.getType()=="Image":
			self.export_image_texture(tex)
		else:
			print "Warning: Can't handle texture of type %s"%(tex.getType())

	def export_image_texture(self, tex):
		self.out.write("imagetexture \"%s\"\n{\n"%(tex.name))
		fn=tex.image.filename
		if self.use_basenames:
			i=0
			for j in range(len(fn)):
				if fn[j]=='/':
					i=j+1
			self.out.write("\tfile \"%s\";\n"%(fn[i:]))
		elif fn[0:2]=="//":
			self.out.write("\tfile \"%s\";\n"%(fn[2:]))
		else:
			self.out.write("\tfile \"%s\";\n"%(fn))
		if tex.extend==Blender.Texture.ExtendModes.REPEAT:
			self.out.write("\twrap both;\n");
		self.out.write("};\n")

	def export_material(self, mat):
		self.out.write("material \"%s\"\n{\n"%(mat.name))
		self.out.write("\tdiffuse %f %f %f;\n"%(mat.R,mat.G,mat.B))
		if mat.spec:
			self.out.write("\tspecular %f %f %f;\n"%(mat.specCol[0]*mat.spec,mat.specCol[1]*mat.spec,mat.specCol[2]*mat.spec))
			self.out.write("\tshininess %d;\n"%(mat.hard))
		if mat.emit:
			self.out.write("\temission %f %f %f;\n"%(mat.R*mat.emit,mat.G*mat.emit,mat.B*mat.emit))
		for i in mat.getTextures():
			if i==None:
				continue
			if i.mapto&Blender.Texture.MapTo.COL:
				self.out.write("\ttexture \"%s\";\n"%(i.tex.name))
			elif i.mapto&Blender.Texture.MapTo.NOR:
				self.out.write("\tbumpmap \"%s\";\n"%(i.tex.name))
		self.out.write("};\n")

	def export_mesh(self, obj):
		print "  Mesh "+obj.name

		self.out.write("object \"%s\"\n{\n"%(obj.name))

		if self.layer:
			self.out.write("\tlayer \"%s\";\n"%self.layer)
			self.out.write("\tposition %f %f %f;\n"%tuple(get_global_location(obj)))
			self.out.write("\trotation %f %f %f %f;\n"%tuple(obj.getMatrix().toQuat()))

		mesh=obj.getData()
		if not mesh.getMode()&Blender.NMesh.Modes.TWOSIDED:
			self.out.write("\tcull;\n")

		vertices=[]
		copies=[]
		for i in mesh.verts:
			v=TddVertex()
			v.p=[i[0],i[1],i[2]]
			vertices.append(v)
			copies.append([])
		autosmooth=mesh.getMode()&Blender.NMesh.Modes.AUTOSMOOTH;
		faces=[]
		materials=mesh.getMaterials()
		unlit=0
		shadow=0
		for i in materials:
			#print hex(i.getMode()),Blender.Material.Modes,Blender.Material.Modes["TRACEABLE"],i.getMode()&Blender.Material.Modes["TRACEABLE"]
			if i.getMode()&Blender.Material.Modes["TRACEABLE"]:
				shadow=1
			if i.getMode()&Blender.Material.Modes["SHADELESS"]:
				unlit=1
		if unlit:
			self.out.write("\tunlit;\n")
		if shadow:
			self.out.write("\tshadow;\n")
		for i in mesh.faces:
			if len(i.v)<3: continue
			f=TddFace()
			for j in i.v:
				f.v.append(mesh.verts.index(j))
			f.smooth=i.smooth
			if i.mat<len(materials):
				f.mat=materials[i.mat].name
			f.normal=i.normal
			faces.append(f)

		if autosmooth:
			self.out.write("\tautosmooth %f;\n"%(mesh.maxSmoothAngle/57.29578))
		else:
			for i in faces:
				if not i.smooth:
					continue
				for j in i.v:
					v=vertices[j]
					if v.n==None:
						v.n=[0,0,0]
					v.n[0]+=i.normal[0]
					v.n[1]+=i.normal[1]
					v.n[2]+=i.normal[2]

		for i in range(len(faces)):
			f=faces[i]
			mf=mesh.faces[i]
			if not len(mf.uv):
				continue;
			for j in range(len(f.v)):
				v=vertices[f.v[j]]
				if v.t==None:
					v.t=mf.uv[j]
				elif (v.t[0]!=mf.uv[j][0]) or (v.t[1]!=mf.uv[j][1]):
					ok=0
					n=f.v[j]
					if vertices[n].l:
						n=vertices[n].l
					for k in copies[n]:
						if vertices[k].t==mf.uv[j]:
							f.v[j]=k
							ok=1
							break
					if not ok:
						v=copy.deepcopy(v)
						v.t=mf.uv[j]
						v.l=f.v[j]
						f.v[j]=len(vertices)
						vertices.append(v)
		for i in vertices:
			if i.n==None:
				continue
			l=math.sqrt(i.n[0]*i.n[0]+i.n[1]*i.n[1]+i.n[2]*i.n[2])
			i.n[0]/=l
			i.n[1]/=l
			i.n[2]/=l
		self.export_vertices(vertices)
		self.export_faces(faces)
		self.out.write("};\n")

	def export_vertices(self, vertices):
		self.out.write("\tvertexcount %d;\n"%(len(vertices)))
		for i in range(len(vertices)):
			v=vertices[i]
			self.out.write("\tvertex\t//%d\n\t{\n"%(i))
			self.out.write("\t\tposition %f %f %f;\n"%tuple(v.p))
			if v.n:
				self.out.write("\t\tnormal %f %f %f;\n"%tuple(v.n))
			if v.t:
				self.out.write("\t\ttexcoords %f %f;\n"%tuple(v.t))
			if v.l!=None:
				self.out.write("\t\tlink %d;\n"%(v.l))
			self.out.write("\t};\n")

	def export_faces(self, faces):
		self.out.write("\tfacecount %d;\n"%(len(faces)))
		mat=None
		for i in faces:
			if i.mat!=mat:
				mat=i.mat
				self.out.write("\tmaterial \"%s\";\n"%(mat))
			self.out.write("\tface\n\t{\n")
			self.out.write("\t\tvertices")
			for j in i.v:
				self.out.write(" %d"%(j))
			self.out.write(";\n")
			if i.smooth:
				self.out.write("\t\tsmooth;\n")
			self.out.write("\t};\n")

	def export_light(self, obj):
		print "  Light "+obj.name
		self.out.write("light \"%s\"\n{\n"%(obj.name))
		light=obj.getData()
		if self.layer:
			self.out.write("\tlayer \"%s\";\n"%self.layer)
		if light.getType()==light.Types["Sun"]:
			self.out.write("\ttype directional;\n")
		elif light.getType()==light.Types["Lamp"]:
			self.out.write("\ttype omni;\n")
		elif light.getType()==light.Types["Spot"]:
			self.out.write("\ttype spot;\n")
			self.out.write("\tcutoff %f %d;\n"%(light.spotSize,light.spotBlend*20))
		self.out.write("\tintensity %f;\n"%(light.energy))
		self.out.write("\tcolor %f %f %f;\n"%tuple(light.col))	#(light.col[0],light.col[1],light.col[2]))
		if light.getType()!=light.Types["Sun"]:
			self.out.write("\tposition %f %f %f;\n"%tuple(get_global_location(obj)))
			self.out.write("\thalfdistance %f;\n"%(light.dist))
		if light.getType()!=light.Types["Lamp"]:
			dir=Blender.Mathutils.Vector([0,0,-1])*obj.matrix.rotationPart()
			self.out.write("\tdirection %f %f %f;\n"%(dir.x,dir.y,dir.z))
		if light.getMode()&8192:	#light.Modes["Shadows"]:
			self.out.write("\tshadow;\n");
		self.out.write("};\n")

	def export_path(self, obj):
		print "  Path "+obj.name
		self.out.write("path \"%s\"\n{\n"%(obj.name))
		loc=get_global_location(obj)
		curve=obj.getData()
		prev=None
		loop=False
		i=0
		while 1:
			try:
				p=curve.getControlPoint(0,i)
				if prev:
					self.out.write("\tpoint control { ")
					self.out.write("position %f %f %f;"%(loc[0]+prev[6],loc[1]+prev[7],loc[2]+prev[8]))
					self.out.write(" };\n")
				if i>0 or loop:
					self.out.write("\tpoint control { ")
					self.out.write("position %f %f %f;"%(loc[0]+p[0],loc[1]+p[1],loc[2]+p[2]))
					self.out.write(" };\n")
				self.out.write("\tpoint curve { ")
				self.out.write("position %f %f %f;"%(loc[0]+p[3],loc[1]+p[4],loc[2]+p[5]))
				self.out.write(" };\n")
				prev=p
				i+=1
				if loop:
					break
			except AttributeError:
				if curve.isCyclic(0):
					loop=True
					i=0
				else:
					break
		self.out.write("};\n")

	def export_camera(self, obj):
		print "  Camera "+obj.name
		self.out.write("camera \"%s\"\n{\n"%(obj.name))
		cam=obj.getData()
		if self.layer:
			self.out.write("\tlayer \"%s\";\n"%self.layer)
			self.out.write("\tposition %f %f %f;\n"%tuple(get_global_location(obj)))
			self.out.write("\trotation %f %f %f %f;\n"%tuple(obj.getMatrix().toQuat()))
		#self.out.write("\tposition %f %f %f;\n"%tuple(get_global_location(obj)))
		#dir=Blender.Mathutils.VecMultMat(Blender.Mathutils.Vector([0,0,-1]),obj.matrix.rotationPart())
		#self.out.write("\tdirection %f %f %f;\n"%(dir.x,dir.y,dir.z))
		self.out.write("\tfieldofview %f;\n"%(math.atan2(12,cam.getLens())*2))
		self.out.write("};\n")

from Blender import Draw,BGL

EVENT_CANCEL=1
EVENT_EXPORT=2
EVENT_OBJECTS=3
EVENT_LIGHTS=4
EVENT_PATHS=5
EVENT_BASENAME=6
EVENT_SELECTED=7
EVENT_CAMERAS=8
EVENT_LAYER=9

exporter=TddExporter()

def draw():
	global btn_layer

	BGL.glClear(BGL.GL_COLOR_BUFFER_BIT)
	BGL.glRasterPos2i(8,184)
	Draw.Text("Three-D Data Export")
	Draw.Toggle("Objects",EVENT_OBJECTS,8,156,100,24,exporter.objects)
	Draw.Toggle("Lights",EVENT_LIGHTS,112,156,100,24,exporter.lights)
	Draw.Toggle("Paths",EVENT_PATHS,8,128,100,24,exporter.paths)
	Draw.Toggle("Cameras",EVENT_CAMERAS,112,128,100,24,exporter.cameras)
	Draw.Toggle("Selected only",EVENT_SELECTED,8,100,204,24,exporter.selected_only)
	Draw.Toggle("Drop paths from filenames",EVENT_BASENAME,8,72,204,24,exporter.use_basenames)
	btn_layer=Draw.String("Layer: ",EVENT_LAYER,8,44,204,24,exporter.layer,50)
	Draw.Button("Cancel",EVENT_CANCEL,8,8,100,32)
	Draw.Button("Export",EVENT_EXPORT,112,8,100,32)

def button_event(event):
	if event==EVENT_CANCEL:
		Draw.Exit()
	elif event==EVENT_EXPORT:
		fn=Blender.sys.makename(ext=".tdd")
		Blender.Window.FileSelector(select_file,"Export TDD",fn)
	elif event==EVENT_OBJECTS:
		exporter.objects=not exporter.objects
	elif event==EVENT_LIGHTS:
		exporter.lights=not exporter.lights
	elif event==EVENT_PATHS:
		exporter.paths=not exporter.paths
	elif event==EVENT_BASENAME:
		exporter.use_basenames=not exporter.use_basenames
	elif event==EVENT_SELECTED:
		exporter.selected_only=not exporter.selected_only
	elif event==EVENT_CAMERAS:
		exporter.cameras=not exporter.cameras
	elif event==EVENT_LAYER:
		exporter.layer=btn_layer.val
	Draw.Redraw()

def select_file(filename):
	exporter.export(filename)
	Draw.Exit()

Draw.Register(draw,None,button_event)
