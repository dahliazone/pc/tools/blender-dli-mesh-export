#!BPY

"""
Name: 'Dahlia mesh'
Blender: 244
Group: 'Export'
Tooltip: 'Dahlia Mesh exporter'
"""
import Blender
import bpy
import struct


class DahliaMeshExporter:

	def __init__(self):
		# Preload object and it's mesh
		scene = bpy.data.scenes.active
		self.obj = scene.objects.active
		if self.obj.getType() != "Mesh":
			# Todo: Throw exception!
			print "ERROR: Selected object must be mesh!"
			return
		self.mesh = self.obj.getData(mesh=1)


	def export(self, filename):
		print "Now exporting..."
		out = open(filename, "w")


		# ------
		# Header
		# ------

		# Identification information
		out.write("DLI-MESH\n")

		# Version
		out.write("a 1.0\n")


		#sce = bpy.data.scenes.active
		#ob = sce.objects.active
		#mesh = ob.getData(mesh=1)


		# --------
		# Material
		# --------
		mat = self.mesh.materials[0]

		# Diffuse
		out.write( 'b %f %f %f\n' % (mat.R, mat.G, mat.B) )

		# Specular
		out.write( 'c %f %f %f\n' % (mat.specCol[0]*mat.spec, mat.specCol[1]*mat.spec, mat.specCol[2]*mat.spec) )

		# Shininess
		out.write( 'd %f\n' % (mat.hard) )

		# Texture
		out.write( 'e your_texture.png\n' )


		# --------
		# Vertices
		# --------

		for vert in self.mesh.verts:
			out.write( 'v %f %f %f\n' % (vert.co.x, vert.co.y, vert.co.z) )
			#oout.write( 'n %f %f %f\n' % (vert.no.x, vert.no.y, vert.no.z) )
			#out.write( 'u %f %f\n' % (vert.uvco.x, vert.uvco.y) )
			#if self.mesh.vertexUV != 0:
			#	out.write( 'u %f %f %f\n' % (vert.uvco.x, vert.uvco.y, vert.uvco.z) )
			#else:
			#	out.write( 'u 0 0 0\n')


		# -----
		# Faces
		# -----
		tris = []
		quads = []
		for face in self.mesh.faces:
			if len(face.v) == 3:
				tris.append(face)
			else:
				quads.append(face)

		for face in tris:
			for vert in face.v:
				out.write( 'n %f %f %f\n' % (vert.no[0], vert.no[1], vert.no[2]) )

			# normal of the face
			#for no in face.no:
			#	out.write( 'n %f %f %f\n' % (no[0], no[1], no[2]) )

			for uv in face.uv:
				out.write( 'u %f %f\n' % (uv[0], uv[1]) )

			out.write('f')
			for vert in face.v:
				out.write( ' %i' % vert.index )
			out.write('\n')

		for face in quads:
			for vert in face.v:
				out.write( 'n %f %f %f\n' % (vert.no[0], vert.no[1], vert.no[2]) )

			# normal of the face
			#for no in face.no:
			#	out.write( 'n %f %f %f\n' % (no[0], no[1], no[2]) )

			for uv in face.uv:
				out.write( 'u %f %f\n' % (uv[0], uv[1]) )

			out.write('f')
			for vert in face.v:
				out.write( ' %i' % vert.index )
			out.write('\n')

		# ------
		# Finish
		# ------

		out.close()
		print "Exporting finished succesfully!"



exporter = DahliaMeshExporter()

fn=Blender.sys.makename(ext=".dli-mesh")
Blender.Window.FileSelector(exporter.export, "Export DLI",fn)

